import './App.css';
import Average from './components/Average';
import Interval from './components/Interval';
import Sort from './components/Sort';
import Sum from './components/Sum';

function App() {

  return (
    <div className="App">
      <h1>React-Redux</h1>
      <div className='line'>
          <Interval />
      </div>
      <div className='line'>
        <Average />
        <Sum />
        <Sort />
      </div>
    </div>
  );
}

export default App;

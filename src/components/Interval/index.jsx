import React from 'react';
import { connect } from 'react-redux';
import { updateMax, updateMin } from '../../store/actions/numbers';
import Card from '../Card';
import './interval.css';

const Interval = props => {
    const {min, max} = props;
    
    return (
        <Card title="Number interval" red>
            <div className='interval'>
                <span>
                    <strong>Minimum</strong>
                    <input type="number" value={min} onChange={e => props.changeMin(+e.target.value)} />
                </span>
                <span>
                    <strong>Maximum</strong>
                    <input type="number" value={max} onChange={e => props.changeMax(+e.target.value)}   />
                </span>
            </div>
        </Card>
    )
}

function mapStateToProps(state) {
    return {
        min: state.numbers.min,
        max: state.numbers.max
    }
}

function mapDispatchToProps(dispatch) {
    return {
        changeMin(newNumber) {
            const action = updateMin(newNumber)
            dispatch(action)
        },
        changeMax(newNumber) {
            const action = updateMax(newNumber)
            dispatch(action)
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Interval)
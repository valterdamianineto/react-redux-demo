import React from 'react';
import { connect } from 'react-redux';
import Card from '../Card';

const Sort = props => {
    const {min, max} = props;
    const randomNumber = Math.floor(Math.random() * (max - min)) + min
    return (
        <Card title="Sort Numbers" purple>
            <div>
                <span>
                    <span>Result: </span>
                    <strong>{randomNumber}</strong>
                </span>
            </div>
        </Card>
    )
}

function mapStateToProps(state) {
    return {
        min: state.numbers.min, 
        max: state.numbers.max
    }
}

export default connect(mapStateToProps)(Sort)
import React from 'react';
import { connect } from 'react-redux';
import Card from '../Card';

const Average = props => {
    const {min, max} = props
    return (
        <Card title="Average of Numbers" blue>
            <div>
                <span>
                    <span>Result: </span>
                    <strong>{(min + max) / 2}</strong>
                </span>
            </div>
        </Card>
    )
}

function mapStateToProps(state) {
    return {
        max: state.numbers.max,
        min: state.numbers.min
    }
}

export default connect(mapStateToProps)(Average)
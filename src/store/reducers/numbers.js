import { MAX_NUM_UPDATED, MIN_NUM_UPDATED } from '../actions/actionTypes'

const initialState = {
    min: 1,
    max: 10
}

export default function (state = initialState, action) {
    switch(action.type){
        case MIN_NUM_UPDATED: 
            return {
                ...state,
                min: action.payload
            }
        case MAX_NUM_UPDATED:
            return {
                ...state,
                max: action.payload
            }
        default: 
            return state
    }
}
export function updateMin(newNumber) {
    return {
        type: 'MIN_NUM_UPDATED', 
        payload: newNumber
    }
}

export function updateMax(newNumber) {
    return {
        type: 'MAX_NUM_UPDATED', 
        payload: newNumber
    }
}

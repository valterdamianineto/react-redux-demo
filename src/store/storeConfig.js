import { combineReducers, createStore } from 'redux';
import NumbersReducer from './reducers/numbers';

const reducers = combineReducers({
    numbers: NumbersReducer
})

function storeConfig() {
    return createStore(reducers)
}

export default storeConfig